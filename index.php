<?php

function reemplazar($text, $textMod, $word, $newWord){

    $fd = fopen($text, 'r+');
    $textModDesc = fopen($textMod, 'x+');

    while (($contenido = fgets($fd)) !== false) {
        $newParagraph = str_replace($word, $newWord, $contenido).'';
        fwrite($textModDesc, $newParagraph);
    }

    fclose($fd);
    fclose($textModDesc);
}

reemplazar('el_quijote.txt', 'el_quijote_modificado.txt', 'Sancho', 'Morty');

